(function ($) {
    $(document).ready(function() {

        // functionality of main menu when hovering
        $('.expanded.dropdown').on('mouseover',function() {
            $(this).children('.dropdown-menu').show();
        }).on('mouseleave', function() {
            $(this).children('.dropdown-menu').hide();
        });

        // link the main menu item with the inline href. If there is not an inline href the menu item will not link somewhere.
        $('.expanded.dropdown').click(function () {
            if ($(this).children('.dropdown-toggle').attr("href") != '') {
                location.href = $(this).children('.dropdown-toggle').attr("href");
            }
        });

/*---------- Here is an implementation of the firts thoughts about the main menu.
  ---------- The code is displaying all the links of the main menu on the header image when the user has not scrolled at all.
  ---------- When the user scrolls, the main menu is shown as usual.

        var headerTop = $('.header-wrapper').offset().top;
        var headerBottom = headerTop;
        $('.dropdown-toggle').click(function () {
            location.href = $(this).attr('href');
        });

        $(window).scroll(function () {
            var scrollTop = $(window).scrollTop(); // Current vertical scroll position from the top
            if (scrollTop > headerBottom) { // Check to see if we have scrolled more than headerBottom
                $('a.dropdown-toggle').addClass('scrolling-toggle');
                $('.dropdown-menu').addClass('scrolling'); // Add class when scrolling
                if (($('.dropdown-menu').hasClass('scrolling') === true)) {
                    //$('.dropdown-menu').css("display", "none");
                    $('.dropdown-menu').hide();
                }

            } else if (scrollTop <= headerBottom || ($(window).width() < 1210)) {
                $('a.dropdown-toggle').removeClass('scrolling-toggle');
                $('.dropdown-menu').removeClass('scrolling');
                //$('.dropdown-menu').css("display", "block");
                $('.dropdown-menu').show();
                $('.dropdown-menu').css("background-color", "initial");
            }
        });
---------- */

        // main menu on responsiveness
        if ($(window).width() < 1210) {
             $('.menu.menu--main-menu').click(function(e) {
                $(this).children('.dropdown').show();
                $(this).addClass('open-main-menu');
             });

            // Hide the main menu when click outside of its div
            // https://stackoverflow.com/questions/1403615/use-jquery-to-hide-a-div-when-the-user-clicks-outside-of-it
            $('body').mouseup(function(e) {
                var container = $('.menu.menu--main-menu').children('.dropdown');
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    container.hide();
                    $('.menu.menu--main-menu').removeClass('open-main-menu');
                }
            });
        }

/*---------- The responsive main menu showing all the links on the header image

               $('.menu.menu--main-menu').click(function (e) {
                    if (e.target === this) {
                        $(this).children('.dropdown').toggle();
                    }
                    $('a.dropdown-toggle').click(function () {
                    if ($(this).hasClass('scrolling-toggle')) {
                        $(this).removeClass('scrolling-toggle');
                    }
                    $(this).next('ul').slideDown(200);
                    $(this).next('ul').children('li').show();
                    if ($('.expanded.dropdown').hasClass('open')) {
                        $('.expanded.dropdown').click(function () {
                            $('.expanded.dropdown.open').children('ul').slideUp(200);
                        });
                    }
                });
               $('.dropdown-toggle').attr("href", "#");
---------- */
    });
}(jQuery));





