(function($) {
    $(document).ready(function(){
        $('.front-line-indico').closest('.block-custom-wrapper').addClass('whats-on-block');
        $('.view-front-page-thumbnail').parent().addClass('front-page-thumbnails');


        //----------------------------- class in content block in landing pages

        if ($('body.page-node-type-landing-page')) {
            $('.component-row__row .block-custom-wrapper .page-content-title').addClass('landing-page-content-block');
        }

        //-----------------------------
    });
}(jQuery));